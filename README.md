Requires Node v13.5.0

To install dependencies:

```npm install```

To run locally:

```NODE_ENV=development PORT=8000 npm start```