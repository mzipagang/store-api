const { search } = require('./store.service');

exports.storeSearch = async (req, reply) => {
    const { code, err, data } = await search(req.body);
    if (err) {
      reply.code(code).send({err});
    } else {
      reply.code(code).send(data);
    }
};