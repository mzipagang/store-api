const controller = require('./store.controller');

const routes = [
  {
    method: 'POST',
    url: '/api/v1/store/search',
    handler: controller.storeSearch,
    schema: {
      description: 'Takes in a postal code and radius and returns stores in the area',
      body: {
        type: 'object',
        properties: {
          postalCode: {
            type: 'string',
            description: 'postal code'
          },
          radius: {
            type: 'integer',
            description: 'search radius'
          },
        }
      },
      response: {
        200: {
          description: 'Successful response',
          type: 'object',
          additionalProperties: true
        }
      }
    }
  }
];

module.exports = routes;
