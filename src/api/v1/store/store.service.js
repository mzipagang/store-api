const { store_url, cached_zipcodes, cached_radius } = require('../../../config');
const { makeQuery } = require('../../../services/graphql');
const { cache, lookup } = require('../../../services/cache');

const LATITUDE = 34.0522;
const LONGITUDE = -118.2437;
// latlng required by storesByLocation query, postalCode not enough
// otherwise throws an error:  "message": "\"value\" must contain at least one of [storeIds, singleLineAddr, latitude]",

const search = async ({radius, postalCode}) => {
  let data;
  const query = (radius, postalCode) => `query StoresByLocation {
    storesByLocation(
      location:{
        radius:${radius},
        postalCode:"${postalCode}",
        latitude:${LATITUDE},
        longitude:${LONGITUDE}
      },
      searchOptions:{
        maxResults:25
      }
    ){
      stores {
        address {
          address,
          city,
          postalCode
        }
      }
    }
  }`;
  try {
    if (cached_zipcodes.includes(postalCode)) {
      data = lookup(postalCode);
      if (!data) {
        data = await makeQuery(store_url, query(cached_radius, postalCode));
        cache(postalCode, data);
      }
    } else {
      data = await makeQuery(store_url, query(radius, postalCode));
    }
    return { code: 200, data };
  } catch (err) {
    return { code: 400, err };
  }
};

module.exports = {
  search
};
