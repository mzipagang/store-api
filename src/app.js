const config = require('./config');
const routes = require('./routes');
const swagger = require('./config/swagger');

// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: {
        level: 'error'
    }
});

fastify.register(require('fastify-cors'), {
    origin: config.origins.includes('_') ? config.origins.split('_') : config.origins.split(','),
    methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
});

// ping endpoint
fastify.get('/', async (request, reply) => {
    return {status: 200}
});

fastify.register(require('fastify-swagger'), swagger.options);

// route setup
routes(fastify);

const start = async () => {
    try {
        await fastify.listen(config.port, '0.0.0.0');
        fastify.swagger();
        console.log(`listening on ${fastify.server.address().port}`)
    } catch (err) {
        console.log(err);
        process.exit(1)
    }
};

start();
