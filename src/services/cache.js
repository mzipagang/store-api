const storeCache = {};

const cache = (postalCode, data) => {
  storeCache[postalCode] = data;
};

const lookup = postalCode => storeCache[postalCode];

module.exports = {
  cache,
  lookup
};