const fetch = require('node-fetch');

const makeQuery = async (url, query) => {
  const results = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query
    })
  });

  return await results.json();
};

module.exports = {
  makeQuery
};