'use strict';

const path = require('path');
const _ = require('lodash');

// All configurations will extend these options
// ============================================
process.env.NODE_ENV =  process.env.NODE_ENV || 'development'
const all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT,

  super_secret: process.env.SUPER_SECRET || 'hippopotamus',

  origins: process.env.ORIGINS || 'http://localhost:3000',

  store_url: 'https://developer.api.stg.walmart.com/api-proxy/service/Store-Services/Store-GraphQL-API/v1/graphql',

  cached_zipcodes: ['97217', '72712', '04210', '02169', '32808'],

  cached_radius: 50
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js'));
