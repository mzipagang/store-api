/**
 * Main application routes
 */

'use strict';

module.exports = fastify => {
  [
    './api/v1/store'
  ].forEach(path => {
  	require(path).forEach(route => {
      fastify.route(route);
    })
  })
}
