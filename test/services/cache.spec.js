const expect = require('chai').expect;
const { cache, lookup } = require('../../src/services/cache');

// only had time to write these unit tests
describe('cache', async function() {
  beforeEach(function(done) {
    done();
  });

  afterEach(function(done) {
    done();
  });

  describe('cache/lookup', async function() {
    beforeEach(function(done) {
      done();
    });

    afterEach(function(done) {
      done();
    });

    it('should cache data', async function() {
      cache('91010', 'data');
      expect(lookup('91010'))
        .to
        .eq('data')
    });

    it('should return undefined for non-cached postal codes', async function() {
      expect(lookup('91344'))
        .to
        .eq(undefined)
    });

    it('should get latest cached data', async function() {
      cache('91010', 'data');
      cache('91010', 'newdata');
      expect(lookup('91010'))
        .to
        .eq('newdata')
    });
  });
});
